#!/usr/bin/python2.7
from __future__ import with_statement
from google.appengine.api import files
import os
import cgi 

from google.appengine.api import images
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.api import urlfetch
import json 
import wsgiref.handlers
import random
import urlparse
import datetime
import time


__author__ = 'no3z'

template.register_template_library('templatetags.basetags')

#------------------------------------------------------------------------------
# Class definitions
#------------------------------------------------------------------------------

#An album class
class Album(db.Model):
  name = db.StringProperty()
  creator = db.UserProperty()
  description = db.TextProperty()
  
  
#A ContentItem class
class ContentItem(db.Model):
  submitter = db.UserProperty()
  submitted_date = db.DateTimeProperty(auto_now_add=True)
  album = db.ReferenceProperty(Album, collection_name='items_set')  
  album_name = db.StringProperty()
  title = db.StringProperty()  
  author = db.StringProperty()
  caption = db.TextProperty()
  comment = db.TextProperty()
  url = db.StringProperty()
  data = db.BlobProperty()
  thumbnail_data = db.BlobProperty()      
  tags = db.CategoryProperty()  
  rating = db.RatingProperty()
  rand = db.FloatProperty()  
  references = db.SelfReferenceProperty()
  isVideo = db.BooleanProperty()
  hasIFrame = db.BooleanProperty()
  iFrame = db.TextProperty()
  ishtmlcontent = db.BooleanProperty()  
  
  
#------------------------------------------------------------------------------
#Funtions
#------------------------------------------------------------------------------

class RequestBaseHandler(webapp.RequestHandler):
  def template_path(self, filename):    
    return os.path.join(os.path.dirname(__file__), filename)

  def render_to_response(self, filename, template_args):    
    template_args.setdefault('current_uri', self.request.uri)
    self.response.out.write(str(
        template.render(self.template_path(filename), template_args)
    ))


#------------------------------------------------------------------------------        
    
class ListAlbums(RequestBaseHandler):  
  def get(self):
    albums = Album.all()
    self.render_to_response('html/list.html', {'albums': albums,})

#------------------------------------------------------------------------------

class CreateAlbum(RequestBaseHandler):
  def get(self):
    self.render_to_response('html/new.html', {})

  def post(self):
    aname = cgi.escape(self.request.get('albumname'))    
    Album(name=aname.upper(),creator=users.get_current_user()).put()
    self.redirect('/list')

#------------------------------------------------------------------------------

class ViewAlbum(RequestBaseHandler):
  def get(self, album_key):
    album = db.get(album_key)
    items = []    
    items = album.items_set    

    self.render_to_response('html/album.html', {        
        'album_key': album.key(),
        'items': items,
        'album_name': album.name
      })

#------------------------------------------------------------------------------      
      
class ServeImage(webapp.RequestHandler):
  def get(self, display_type, pic_key):
    image = db.get(pic_key)

    if display_type == 'image':
      self.response.headers['Content-Type'] = 'image/png'
      self.response.out.write(str(image.data))
    elif display_type == 'thumbnail':
      self.response.headers['Content-Type'] = 'image/png'
      self.response.out.write(str(image.thumbnail_data))
    else:
      self.error(500)
      self.response.out.write('Couldn\'t determine what type of image to serve.')

#------------------------------------------------------------------------------

class ShowItem(RequestBaseHandler):
  def get(self, item_key):
    item = db.get(item_key)
    self.render_to_response('html/show_item.html', {
        'item': item,
        'image_key': item.key(),
        'album' : item.album,
    })

#------------------------------------------------------------------------------

class FeatureItem(RequestBaseHandler):
  def get(self, item_key):
    item = db.get(item_key)
    item.submitted_date = datetime.datetime.now()
    item.put()
    self.redirect('/show_image/%s' % item_key)

#------------------------------------------------------------------------------

class ShowImage(RequestBaseHandler):
  def get(self, item_key):
    item = db.get(item_key)
    self.render_to_response('html/show_image.html', {
        'item': item,
        'image_key': item.key(),
        'album' : item.album,
    })

  def post(self, item_key):
    item = db.get(item_key)    
    if item is None:
      self.error(400)
      self.response.out.write('Couldn\'t find specified item')
    
    title = cgi.escape(self.request.get('title'))
    url = cgi.escape(self.request.get('url'))
    caption = self.request.get('caption')
    comment = self.request.get('comment')
    author = cgi.escape(self.request.get('author'))    
    tags = cgi.escape(self.request.get('tags'))    
    album_name = cgi.escape(self.request.get('album_name'))
    iFrame =  self.request.get('iFrame')

    item.title = title
    item.caption = caption
    item.comment = comment
    item.author = author
    item.url = url    
    item.album_name = album_name.upper()
    item.iFrame = iFrame
    
    item.ishtmlcontent = False
    if self.request.get('ishtmlcontent') == 'true':
      item.ishtmlcontent = True         
        
    item.isVideo = False
    if self.request.get('isVideo') == 'true':
      item.isVideo = True   

    item.hasIFrame = False
    if self.request.get('hasIFrame') == 'true':
      item.hasIFrame = True
      
    img_data = False             
    if self.request.POST.get('picfile'):		
      img_data = self.request.POST.get('picfile').file.read()
    
    if img_data:
      try:
	img = images.Image(img_data)
	img.im_feeling_lucky()
	png_data = img.execute_transforms(images.PNG)
	img.resize(300,169)	
	thumbnail_data = img.execute_transforms(images.PNG)
	
	item.data=png_data      
	item.thumbnail_data=thumbnail_data
      except:
	self.error(400)
	self.response.out.write("ERROR reading image")
	
    item.put()

    self.redirect('/album/%s' % item.album.key())
      

#------------------------------------------------------------------------------

class DeleteImage(RequestBaseHandler):
  def get(self, item_key):
    item = db.get(item_key)  
    album = item.album    
    if item is None:
      self.error(400)
      self.response.out.write('Couldn\'t find specified item')        
    item.delete()
    self.redirect('/album/%s' % album.key())
      
#------------------------------------------------------------------------------

class GetRedditFeed(RequestBaseHandler):
  def get(self, album_key, feed):
    album = db.get(album_key)
    feed_name = feed
    if album is None:
      self.error(400)
      self.response.out.write('Couldn\'t find specified album')
	  
    page_json = urlfetch.Fetch('http://www.reddit.com/r/'+feed_name+'.json' ) 	            
    obj = json.loads(  page_json.content )           
    if not obj.get('data').get('children'):
      self.response.out.write(obj.get('data'))
      return    
    for subs in  obj.get('data').get('children'):
      if not subs['data']['url']:
	continue      
      path = urlparse.urlparse(subs['data']['url']).path
      ext = os.path.splitext(path)[1]
      title = subs['data']['title'].replace("\"", "\'")
      try:
	image = urlfetch.Fetch(subs['data']['url']).content
	img = images.Image(image)
	img.im_feeling_lucky()
	
	if img.width > 1920 or img.height > 1080:
	  img.resize(img.width/2,img.height/2)

	png_data = img                
	png_data = img.execute_transforms(images.PNG)
	
	img.resize(300,169)
	thumbnail_data = img.execute_transforms(images.PNG)	
	self.response.out.write("About to put item: " + title)
	
	ContentItem(submitter=users.get_current_user(),
		title=title,
		comment=subs['data']['subreddit'],
		album=album,
		album_name=album.name,
		url=subs['data']['url'],
		data=png_data,
		caption=title,
		author= subs['data']['author'],
		rand = random.random(),
		ishtmlcontent = False,				
		rating = 0,
                isVideo = False,
                hasIFrame = False,
		tags=album.name,
		thumbnail_data=thumbnail_data).put()	     			 	  
      except:
	    self.error(400)
	    self.response.out.write("ERROR")
    
    self.redirect('/album/%s' % album.key())  	      
      

#------------------------------------------------------------------------------

class GetYouTubeFeed(RequestBaseHandler):   
  def get(self, album_key, feed):
    album = db.get(album_key)
    feed_name = feed
    if album is None:
      self.error(400)
      self.response.out.write('Couldn\'t find specified album')	  
    page_json = urlfetch.Fetch('https://gdata.youtube.com/feeds/api/videos?q='+feed_name+'&v=2&alt=jsonc')        	  
    obj = json.loads(  page_json.content )                  
    for subs in obj.get('data').get('items'):	      
      result = urlfetch.fetch(subs['thumbnail']['hqDefault'])
      if result.status_code == 200:
	image = result.content	  
	img = images.Image(image)
	img.im_feeling_lucky()
	png_data = img                
	png_data = img.execute_transforms(images.PNG)	 
      result2 = urlfetch.fetch(subs['thumbnail']['sqDefault'])
      if result2.status_code == 200:
	image2 = result2.content	  
	img2 = images.Image(image2)
	img2.im_feeling_lucky()
	tb_data = img2.execute_transforms(images.PNG)
      ContentItem(submitter=users.get_current_user(),
	title=subs['title'],
	comment=subs['category'],
	album=album,
	album_name=album.name,
	url=subs['content']['5'],
	data=png_data,
	caption=subs['description'],
	author= subs['uploader'],
	rand = random.random(),
	ishtmlcontent = False,	
        isVideo = True,
        hasIFrame = False,
	rating = 0,
	tags=album.name,
	thumbnail_data=tb_data).put()	     	    
      self.response.out.write("Add: " + subs['title'] + " correct!<br>")
           

#------------------------------------------------------------------------------

class UploadImageToAlbum(RequestBaseHandler):
  def get(self, album_key):
    album = db.get(album_key)
    self.render_to_response('html/upload.html', {
        'album_key': album.key(),
        'album_name': album.name
      })

  def post(self, album_key):    
    album = db.get(album_key)
    if album is None:
      self.error(400)
      self.response.out.write('Couldn\'t find specified album')

    title = cgi.escape(self.request.get('title'))
    url = cgi.escape(self.request.get('url'))
    caption = self.request.get('caption')
    comment = self.request.get('comment')
    author = cgi.escape(self.request.get('author'))    
    album_name = cgi.escape(self.request.get('album_name'))
    iFrame =  cgi.escape(self.request.get('iFrame'))
    
    isVideo = False
    if self.request.get('isVideo') == 'true':
      isVideo = True   

    hasIFrame = False
    if self.request.get('hasIFrame') == 'true':
      hasIFrame = True

    img_data = self.request.POST.get('picfile').file.read()
    
    ishtml = False
    if self.request.get('ishtmlcontent') == 'true':
      ishtml = True                 

    try:
      img = images.Image(img_data)
      img.im_feeling_lucky()
      png_data = img.execute_transforms(images.PNG)
      img.resize(300, 170)
      thumbnail_data = img.execute_transforms(images.PNG)
      ContentItem(submitter=users.get_current_user(),
              title=title,
              caption=caption,
              comment=comment,
              album=album,
              album_name=album.name,
              tags=album.name,
              url=url,
              author=author,
              rand = random.random(),
              data=png_data,
              ishtmlcontent=ishtml,
              isVideo=isVideo,
              hasIFrame=hasIFrame,
              iFrame=iFrame,                  
              thumbnail_data=thumbnail_data).put()
                                    
      self.redirect('/album/%s' % album.key())
    except images.BadImageError:
      self.error(400)
      self.response.out.write(
          'Sorry, we had a problem processing the image provided.')
    except images.NotImageError:
      self.error(400)
      self.response.out.write(
          'Sorry, we don\'t recognize that image format.'
          'We can process JPEG, GIF, PNG, BMP, TIFF, and ICO files.')
    except images.LargeImageError:
      self.error(400)
      self.response.out.write(
          'Sorry, the image provided was too large for us to process.')



#------------------------------------------------------------------------------

class serveIndexPage(RequestBaseHandler):
  def get(self):
    latest = ContentItem.all().order("-submitted_date").fetch(3)
    project = ContentItem.all().filter("album_name =", "PROJECTS").order("-submitted_date").fetch(3)
    music = ContentItem.all().filter("album_name =", "MUSIC").order("-submitted_date").fetch(3)
    video = ContentItem.all().filter("album_name =", "VIDEO").order("-submitted_date").fetch(3)    
    links = ContentItem.all().filter("album_name =", "LINKS").order("-submitted_date").fetch(3)      
    about = ContentItem.all().filter("album_name =", "ABOUT").order("-submitted_date").fetch(3)            
    
    self.render_to_response('html/index.html', {
        'latest1': latest[0],
        'latest2': latest[1],
        'latest3': latest[2], 
        'project': project, 
        'music': music,
	'video': video, 
        'links': links, 
        'about': about,
      })

#------------------------------------------------------------------------------

class ReturnAdminPage(RequestBaseHandler):
  def get(self):       
    self.render_to_response('html/admin.html',{})
    
#------------------------------------------------------------------------------

class ReturnVideoAlbumImages(RequestBaseHandler):
  def get(self):   
    items = []
    items = ContentItem.all()  
    items.filter("album_name =", "VIDEO")      
    items.order("-submitted_date")
    album = Album.all().filter("name =", "VIDEO")
    self.render_to_response('html/content.html', {'items': items,'album': album[0]})

    
#------------------------------------------------------------------------------

class ReturnMusicAlbumImages(RequestBaseHandler):
  def get(self):   
    items = []
    items = ContentItem.all()  
    items.filter("album_name =", "MUSIC")      
    items.order("-submitted_date")
    album = Album.all().filter("name =", "MUSIC")
    self.render_to_response('html/content.html', {'items': items, 'album': album[0]})

#------------------------------------------------------------------------------

class ReturnLinksAlbumImages(RequestBaseHandler):
  def get(self):   
    items = []
    items = ContentItem.all()  
    items.filter("album_name =", "LINKS")      
    items.order("-submitted_date")
    album = Album.all()
    album.filter("name =", "LINKS")
    self.render_to_response('html/content.html', {'items': items, 'album': album[0]})

#------------------------------------------------------------------------------

class ReturnAboutAlbumImages(RequestBaseHandler):
  def get(self):        
    items = []
    items = ContentItem.all()  
    items.filter("album_name =", "ABOUT")      
    items.order("-submitted_date")
    album = Album.all()
    album.filter("name =", "ABOUT")
    self.render_to_response('html/content.html', {'items': items, 'album': album[0]})

#------------------------------------------------------------------------------

class ReturnProjectAlbumImages(RequestBaseHandler):
  def get(self):   
    items = []
    items = ContentItem.all()  
    items.filter("album_name =", "PROJECTS")      
    items.order("-submitted_date")
    album = Album.all().filter("name =", "PROJECTS")
    self.render_to_response('html/content.html', {'items': items,'album': album[0]})
    
#------------------------------------------------------------------------------

class showTerms(RequestBaseHandler):
  def get(self):   
     self.render_to_response('html/terms.html', {})
    
#------------------------------------------------------------------------------

class RemoveAllImageFromAlbum(RequestBaseHandler):
  def get(self, album_key):     
    album = db.get(album_key)
    items = []
    items = ContentItem.all()
    items.filter("album =", album)
    for item in  items:
      self.response.out.write("About to delete: " +item.title)
      item.delete()
      self.response.out.write(" Deleted! <br>")

#------------------------------------------------------------------------------


class RemoveAlbum(RequestBaseHandler):
  def get(self, album_key):     
    album = db.get(album_key)
    items = []
    items = ContentItem.all()
    items.filter("album =", album)
    for item in  items:    
	item.delete()
	self.response.out.write("Deleting : " +item.title + "<br>")	
	
    album.delete();		
    self.response.out.write("Album deleted")	
          
#------------------------------------------------------------------------------

class updateFeeds(RequestBaseHandler):
  def get(self, album, feed):    
    self.response.out.write(album + " " + feed)
    albums = Album.all()
    for t in albums:
      if(t.name == album):
	form_data= { "feed_name" : feed }
	key = str(t.key())
	durl = "/redditfeed/" + key + "/" + feed
	self.redirect(durl) 	  	    
     
  def post(self, album_key, none):    
    feed_name = cgi.escape(self.request.get('reddit_feed'))         
    self.redirect("/redditfeed/" + album_key + "/" + feed_name)
    
#------------------------------------------------------------------------------

class updateYoutube(RequestBaseHandler):
  def get(self, album, feed):    
    self.response.out.write(album + " " + feed)
    albums = Album.all()
    for t in albums:
      if(t.name == album):
	form_data= { "feed_name" : feed }
	key = str(t.key())
	durl = "/youtubefeed/" + key + "/" + feed
	self.redirect(durl) 
     
  def post(self, album_key, none):    
    feed_name = cgi.escape(self.request.get('youtube_feed'))           
    self.redirect("/youtubefeed/" + album_key + "/" + feed_name)      
          
#------------------------------------------------------------------------------
# URI handler
#------------------------------------------------------------------------------

    
def main():
  url_map = [('/list', ListAlbums),
             ('/new', CreateAlbum),
             ('/album/([-\w]+)', ViewAlbum),
             ('/upload/([-\w]+)', UploadImageToAlbum),         
             ('/show_image/([-\w]+)', ShowImage),
             ('/show_item/([-\w]+)', ShowItem),
             ('/feature_item/([-\w]+)', FeatureItem),
             ('/update/([-\w]+)', ShowImage),
	     ('/delete_image/([-\w]+)', DeleteImage),
	     ('/remove_all_album_images/([-\w]+)', RemoveAllImageFromAlbum),
	     ('/remove_album/([-\w]+)', RemoveAlbum),	     
	     ('/updateReddit/([-\w]+)/([-\w]+)', updateFeeds),
	     ('/updateYoutube/([-\w]+)/([-\w]+)', updateYoutube),
	     ('/redditfeed/([-\w]+)/([-\w]+)', GetRedditFeed),
             ('/youtubefeed/([-\w]+)/([-\w]+)', GetYouTubeFeed),   
             ('/(thumbnail|image)/([-\w]+)', ServeImage),
	     ('/projects', ReturnProjectAlbumImages),
             ('/about', ReturnAboutAlbumImages),
             ('/links', ReturnLinksAlbumImages),
             ('/video', ReturnVideoAlbumImages),
	     ('/music', ReturnMusicAlbumImages),
	     ('/admin', ReturnAdminPage),
	     ('/terms', showTerms),
             ('/', serveIndexPage)]
  
  application = webapp.WSGIApplication(url_map,debug=True)
  
  wsgiref.handlers.CGIHandler().run(application)

#------------------------------------------------------------------------------

if __name__ == '__main__':
  main()
